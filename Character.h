#ifndef CHARACTER_H
#define CHARACTER_H
#include "Map.h"
#include <iostream>


enum Direction
{
    UP,
    DOWN,
    LEFT,
    RIGHT,
    NONE
};

class Character
{
public:
    Character();
    Character(int horizontalBlock, int verticalBlock);
    int getBlockX();
    int getBlockY();
    void move(Direction direction);
    void move(Direction direction, Map &map);
    virtual ~Character();
protected:
    int block_x;
    int block_y;
private:
};

#endif // CHARACTER_H
