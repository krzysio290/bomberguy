#include "Bomberguy.h"
#define blockSize 50
Bomberguy::Bomberguy()
{
    Bomberguy(1,1);
}
Bomberguy::Bomberguy(int x, int y)
{
    block_x = x;
    block_y = y;
}

Bomberguy::~Bomberguy()
{

}

void Bomberguy::setX(int x)
{
    block_x = x;
}

void Bomberguy::setY(int y)
{
    block_y = y;
}

int Bomberguy::getX()
{
    return block_x;
}
int Bomberguy::getY()
{
    return block_y;
}
