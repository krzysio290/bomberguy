#include "Bomb.h"

Bomb::Bomb()
{
    timer = 100; //should be 2 seconds
}

Bomb::Bomb(int ticks)
{
    timer = ticks;
}

Bomb::~Bomb()
{
    //dtor
}
void Bomb::arm()
{
    armed = true;
}
bool Bomb::isArmed()
{
    return armed;
}
void Bomb::disarm()
{
    armed = false;
}
int Bomb::getTimeLeft()
{
    return timer;
}

void Bomb::tick()
{
    if(timer>0)
        timer--;
}

void Bomb::setX(int x)
{
    this->x = x;
}
void Bomb::setY(int y)
{
    this->y = y;
}
int Bomb::getX()
{
    return x;
}

int Bomb::getY()
{
    return y;
}
