#ifndef BOMBERGUY_H
#define BOMBERGUY_H
#include "Character.h"

class Bomberguy : public Character
{
public:
    Bomberguy();
    Bomberguy(int x, int y);
    void setX(int x);
    void setY(int y);
    int getX();
    int getY();
    virtual ~Bomberguy();
protected:
private:
};

#endif // BOMBERGUY_H
