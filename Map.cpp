#include "Map.h"
//#define LOG
Map::Map()
{
    for(int i = 0; i < 50; i++)
        for(int j = 0; j < 50; j++)
            objectsOnMap[i][j] = 0;
}

Map::~Map()
{
    for(unsigned int i = 0; i<enemiesCoords.size(); i++)
    {
        delete enemiesCoords[i];
    }
    enemiesCoords.clear();
}

void Map::readMapFile(std::string filename)
{
    for(unsigned int i = 0; i<enemiesCoords.size(); i++)
    {
        delete enemiesCoords[i];
    }
    enemiesCoords.clear();
    int enemies;
    std::ifstream file;
    file.open(filename.c_str());
    if(file.is_open())
    {
        std::cout << "File open" << std::endl;
        file >> width;
        file >> height;
        for(int y = 0; !file.eof() && y < height; y++)
        {
            for(int x = 0; !file.eof() && x < width; x++)
                file >> objectsOnMap[x][y];
        }
        file >> enemies;
        for(int i=0; i<enemies; i++)
        {
            EnemyPosition *pos = new EnemyPosition;
            file >> pos->x;
            file >> pos->y;
            enemiesCoords.push_back(pos);
        }

    }
#ifdef LOG
    for(int x = 0; x < width; x++)
    {
        for(int y = 0; y < height; y++)
        {
            std::cout << objectsOnMap[x][y] << " ";
        }
        std::cout << std::endl;
    }
#endif
    file.close();
}

int Map::getX()
{
    return width;
}
int Map::getY()
{
    return height;
}
int Map::getObjectXY(int x, int y)
{
    return objectsOnMap[x][y];
}

int Map::getEnemiesNumber()
{
    return enemiesCoords.size();
}
int Map::getEnemyX(int number)
{
    return enemiesCoords[number]->x;
}
int Map::getEnemyY(int number)
{
    return enemiesCoords[number]->y;
}


















