#ifdef __cplusplus
#include <cstdlib>
#else
#include <stdlib.h>
#endif
#include <string>
#include <vector>
#include <SDL/SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include "Map.h"
#include "Game.h"
#include "Bomb.h"
#include "Timer.h"

const int resolutionX = 750;
const int resolutionY = 400;
const int blockSize   = 50;
const int FRAMES_PER_SECOND = 50;
enum objectType
{
    grass      = 0,
    concrete   = 1,
    brick      = 2,
    brick_hard = 3,
    bomberguy  = 4,
    bomb       = 5,
    enemy      = 6
};
enum explosionParts
{
    center      = 0,
    up          = 1,
    right       = 2,
    down        = 3,
    left        = 4,
    up_end      = 5,
    right_end   = 6,
    down_end    = 7,
    left_end    = 8
};
SDL_Surface *load_image(std::string filename);
void addSprite(SDL_Surface *source, SDL_Surface* destination, int x, int y);
void drawMap(SDL_Surface *screen, Map &definition, SDL_Surface **objects);
void drawGame(SDL_Surface *screen, Game &game, SDL_Surface **objects);
void drawExplosions(SDL_Surface *screen, Game& game, SDL_Surface **parts);
void drawStartMenu(SDL_Surface *screen, int activePosiotion);
void gameReset(Game *game, Map &mapunia, int &levelCounter, char fileMaps[][10]);
void nextLevel(Game *game, Map &mapunia, int &levelCounter, char fileMaps[][10]);
int main ( int argc, char** argv )
{
    Timer fps;
    int menuChoice = 0;
    SDL_Surface **objects = new SDL_Surface*[10];
    objects[0] = IMG_Load("grass.png");
    objects[1] = IMG_Load("concrete.png");
    objects[2] = IMG_Load("brick_soft.png");
    objects[3] = IMG_Load("brick_hard.png");
    objects[4] = IMG_Load("bomber.png");
    objects[5] = IMG_Load("bomb.png");
    objects[6] = IMG_Load("enemy.png");
    SDL_Surface **explosionPart = new SDL_Surface*[9];
    explosionPart[center]   = IMG_Load("fire_center.png");
    explosionPart[up]       = IMG_Load("vectical.png");
    explosionPart[right]    = IMG_Load("horizontal.png");
    explosionPart[down]     = IMG_Load("vectical.png");
    explosionPart[left]     = IMG_Load("horizontal.png");
    explosionPart[up_end]   = IMG_Load("fire_up.png");
    explosionPart[right_end]= IMG_Load("fire_right.png");
    explosionPart[down_end] = IMG_Load("fire_down.png");
    explosionPart[left_end] = IMG_Load("fire_left.png");

    TTF_Font *font = NULL;
    Mix_Chunk *boomSound = NULL;
    // initialize SDL video
    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "Unable to init SDL: %s\n", SDL_GetError() );
        return 1;
    }
    Mix_Init(MIX_INIT_MP3);
    IMG_Init(IMG_INIT_PNG);
    atexit(SDL_Quit);

    SDL_Surface* screen = SDL_SetVideoMode(resolutionX, resolutionY, 24, SDL_HWSURFACE|SDL_DOUBLEBUF);

    if ( !screen )
    {
        printf("Unable to set 1366x768 video: %s\n", SDL_GetError());
        return 1;
    }
    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024)==-1)
    {
        printf("Mix_OpenAudio: %s\n", Mix_GetError());
        exit(2);
    }
    boomSound = Mix_LoadWAV("ex.wav");
//    Mix_PlayChannel(1, boomSound, 0);
    if( TTF_Init() == -1 )
        return false;
    font = TTF_OpenFont( "font.ttf", 18 );
    if( font == NULL )
    {
        return false;
    }

    char levels[3][10] = {"map1.dat", "map2.dat", "map3.dat"};
    int levelActive = 0;
    Map mapka;
    Game *game = new Game();
    gameReset(game, mapka, levelActive,levels);
    std::vector<Bomb*> bombki;
    Direction bomberguyDirection = NONE;

    // program main loop
    bool done = false;
    bool pause = false;
    int gameStatus = 0; ///0 = bez rozstrzygniecia, 1 = wygrana, -1 = przegrana

    while (!done)
    {
        game->setBoomSound(boomSound);
        if(!pause)
        {
            switch(game->isWinner())
            {
            case 0:
                gameStatus = 0;
                break;
            case 1:
                gameStatus = 1;
                pause = true;
                std::cout << "Win!\n";
                break; //Sure! He has.
            case -1:
                gameStatus = -1;
                pause = true;
                std::cout << "Game over should be\n";
                break; //Father noooooo!
            }
        }


        // message processing loop
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            // check for messages
            switch (event.type)
            {
            case SDL_QUIT:
                done = true;
                break;

                // check for keypresses
            case SDL_KEYDOWN:
            {
                if (event.key.keysym.sym == SDLK_ESCAPE)
                {
                    menuChoice = 0;
                    if(gameStatus==0)
                        pause = !pause;

                    if(pause)
                        Mix_Pause(-1);
                    else
                        Mix_Resume(-1);

                }
                if (event.key.keysym.sym == SDLK_UP)
                {
                    if(!pause)
                    {
                        bomberguyDirection = UP;
                    }
                    else
                    {
                        if(menuChoice>0) menuChoice--;
                    }
                }

                if (event.key.keysym.sym == SDLK_DOWN)
                {
                    if(!pause)
                    {
                        bomberguyDirection = DOWN;
                    }
                    else
                    {
                        if(menuChoice<2) menuChoice++;
                    }
                }

                if(!pause)
                {
                    if (event.key.keysym.sym == SDLK_LEFT)
                        bomberguyDirection = LEFT;
                    if (event.key.keysym.sym == SDLK_RIGHT)
                        bomberguyDirection = RIGHT;
                    if (event.key.keysym.sym == SDLK_SPACE)
                    {
                        std::cout << "Bombs away!" << std::endl;
                        game->addBomb(game->getBomberguy()->getBlockX(),game->getBomberguy()->getBlockY());
                    }
                }
                else
                {
                    if(event.key.keysym.sym == SDLK_RETURN)
                    {
                        if(gameStatus==0)
                        {
                            switch(menuChoice)
                            {
                            case 0:   ///Resume
                            {
                                pause = false;
                                break;
                            }
                            case 1:   ///New gejm
                            {
                                ///A TUTAJ TO BEDZIE MEKSYK
                                std::cout << "New game choosen" << std::endl;
                                gameReset(game, mapka, levelActive,levels);
                                pause = false;
                                break;
                            }
                            case 2:   ///Exit
                            {
                                done = true;
                                break;
                            }
                            }
                        }
                        else     ///KIEDY GRA SIE ZAKONCZYLA
                        {
                            ///DAJ NJU GEJM
                            if(gameStatus==1)
                            {
                                nextLevel(game, mapka, levelActive,levels);
                                pause = false;
                            }
                            else
                            {
                                gameReset(game, mapka, levelActive,levels);
                                pause = false;
                            }

                        }

                    }
                }
                //jebnij bombe
                break;
            }
            case SDL_KEYUP:
            {
                switch(event.key.keysym.sym)
                {
                case SDLK_UP:
                case SDLK_DOWN:
                case SDLK_LEFT:
                case SDLK_RIGHT:
                    std::cout << "NONE\n";
                    bomberguyDirection = NONE;
                    break;
                default:
                    break;
                }
            }

            } // end switch

        } // end of message processing
        if(!pause)
        {

            ///Sprawdzic mozliwosc i ewentualnie ruszyc bombermanem
            switch(bomberguyDirection)
            {
            case UP:
                if(game->checkInBounds(game->getBomberguy()->getBlockX(), game->getBomberguy()->getBlockY()-1, mapka.getX(), mapka.getY()))
                {
                    game->getBomberguy()->move(UP, mapka);
                }
                else
                {
                    std::cout << "Out of bounds!" << std::endl;
                }
                break;
            case DOWN:
                if(game->checkInBounds(game->getBomberguy()->getBlockX()+1, game->getBomberguy()->getBlockY()+1, mapka.getX(), mapka.getY()))
                {
                    game->getBomberguy()->move(DOWN, mapka);
                }
                else
                {
                    std::cout << "Out of bounds!" << std::endl;
                }
                break;
            case LEFT:
            {
                if(game->checkInBounds(game->getBomberguy()->getBlockX()-1, game->getBomberguy()->getBlockY(), mapka.getX(), mapka.getY()))
                {
                    game->getBomberguy()->move(LEFT, mapka);
                }
                else
                {
                    std::cout << "Out of bounds!" << std::endl;
                }
                break;
            }
            case RIGHT:
            {
                if(game->checkInBounds(game->getBomberguy()->getBlockX()+1, game->getBomberguy()->getBlockY(), mapka.getX(), mapka.getY()))
                {
                    game->getBomberguy()->move(RIGHT, mapka);
                }
                else
                {
                    std::cout << "Out of bounds!" << std::endl;
                }
                break;
            }
            default:
                break;
            }
        }

        // DRAWING STARTS HERE
        SDL_FillRect(screen, 0, SDL_MapRGB(screen->format, 0, 0, 0));  // clear screen
        if(!pause)
        {
            drawMap(screen, mapka, objects);
            drawGame(screen, *game, objects);
            drawExplosions(screen, *game, explosionPart);
            game->tick();
        }
        else
        {
            ///RENDEROWANIE PAUZY
            if(gameStatus==0)
            {
                SDL_Color textColor = { 255, 255, 255 };
                SDL_Color textColorSelected = {255, 0, 0};
                SDL_Surface *message = NULL;
                char messageTab[3][25] = {"Resume", "New game", "Exit"};
                for(int i = 0; i < 3; i++)
                {
                    message = TTF_RenderText_Solid( font, messageTab[i], (menuChoice == i ? textColorSelected : textColor) );
                    addSprite(message, screen, 50,50+i*50);
                    SDL_FreeSurface( message );
                }
            }
            else if(gameStatus==1)   ///Wygrana
            {
                SDL_Color textColorSelected = {0, 255, 0};
                SDL_Surface *message = NULL;
                message = TTF_RenderText_Solid( font, "You win!", textColorSelected);
                addSprite(message, screen, 100,50);
                SDL_FreeSurface( message );

            }
            else if(gameStatus==-1)   ///Przegrana
            {
                SDL_Color textColorSelected = {255, 0, 0};
                SDL_Surface *message = NULL;
                message = TTF_RenderText_Solid( font, "GAME OVER!", textColorSelected);
                addSprite(message, screen, 100,50);
                SDL_FreeSurface( message );

            }
        }


        // DRAWING ENDS HERE
        SDL_Flip(screen);
        //Cap the frame rate
        if( fps.get_ticks() < 1000 / FRAMES_PER_SECOND )
        {
            SDL_Delay( ( 1000 / FRAMES_PER_SECOND ) - fps.get_ticks() );
        }
    }

    for(int i = 0; i < 7; i++)
        SDL_FreeSurface(objects[i]);
    delete objects;
    for(unsigned int i = 0; i < 9; i++)
        SDL_FreeSurface(explosionPart[i]);
    delete explosionPart;
    //Close the font that was used
    Mix_FreeChunk(boomSound);
    TTF_CloseFont( font );
    Mix_CloseAudio();
    Mix_Quit();
    //Quit SDL_ttf
    TTF_Quit();
    IMG_Quit();

    printf("Exited cleanly\n");
    return 0;
}

SDL_Surface *load_image( std::string filename )
{
    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;
    loadedImage = IMG_Load( filename.c_str() );
    if( loadedImage != NULL )
    {
        optimizedImage = SDL_DisplayFormat( loadedImage );
        SDL_FreeSurface( loadedImage );
    }
    return optimizedImage;
}

void addSprite(SDL_Surface *source, SDL_Surface* destination, int x, int y)
{
    SDL_Rect dstrect;
    dstrect.x = x;
    dstrect.y = y;
    SDL_BlitSurface(source, 0, destination, &dstrect);
}
void drawMap(SDL_Surface *screen, Map &definition, SDL_Surface **objects)
{
    for(int y = 0; y < definition.getY(); y++)
    {
        for(int x = 0; x < definition.getX(); x++)
        {
            addSprite(objects[definition.getObjectXY(x,y)],screen,x*blockSize,y*blockSize);
        }
    }
}
void drawExplosions(SDL_Surface *screen, Game& game, SDL_Surface **parts)
{
    for(unsigned int i = 0; i < game.getExplosionsNumber(); i++)
    {
        int expl_x = game.getExplosion(i)->x;
        int expl_y = game.getExplosion(i)->y;
        addSprite(parts[center],    screen, expl_x,     expl_y);
        addSprite(parts[up_end],    screen, expl_x,     expl_y-100);
        addSprite(parts[up],        screen, expl_x,     expl_y-50);
        addSprite(parts[down],      screen, expl_x,     expl_y+50);
        addSprite(parts[down_end],  screen, expl_x,     expl_y+100);
        addSprite(parts[right],     screen, expl_x+50,  expl_y);
        addSprite(parts[right_end], screen, expl_x+100, expl_y);
        addSprite(parts[left],      screen, expl_x-50,  expl_y);
        addSprite(parts[left_end],  screen, expl_x-100, expl_y);
    }
}
void drawGame(SDL_Surface *screen, Game& game, SDL_Surface **objects)
{
    for(unsigned int i = 0; i < game.getBombsNumber(); i++)
    {
        if(game.getBomb(i)->getTimeLeft()!=0)
        {
            addSprite(objects[bomb],screen,game.getBomb(i)->getX(), game.getBomb(i)->getY());
        }
    }
    addSprite(objects[bomberguy],screen,game.getBomberguy()->getX(),game.getBomberguy()->getY());
    for(int i = 0; i < game.getEnemiesNumber(); i++)
    {
        addSprite(objects[enemy],screen,game.getEnemyX(i),game.getEnemyY(i));
    }
}
void gameReset(Game *game, Map &mapunia, int &levelCounter, char fileMaps[][10])
{
    delete game;
    game = new Game();
    mapunia.readMapFile(fileMaps[levelCounter]);
    game->setBomberGuy(1,1);
    for(int i = 0; i < mapunia.getEnemiesNumber(); i++)
    {
        game->addEnemy(mapunia.getEnemyX(i),mapunia.getEnemyY(i));
    }

}
void nextLevel(Game *game, Map &mapunia, int &levelCounter, char fileMaps[][10])
{
    delete game;
    game = new Game();
    levelCounter++;
    mapunia.readMapFile(fileMaps[levelCounter]);
    game->setBomberGuy(1,1);
    for(int i = 0; i < mapunia.getEnemiesNumber(); i++)
    {
        game->addEnemy(mapunia.getEnemyX(i),mapunia.getEnemyY(i));
    }

}
