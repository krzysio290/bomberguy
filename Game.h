#ifndef GAME_H
#define GAME_H
#include <vector>
#include <iostream>
#include <cstdlib>
#include <SDL_mixer.h>
#include "Enemy.h"
#include "Bomberguy.h"
#include "Character.h"
#include "Map.h"
#include "Bomb.h"
#define LOG
//#define blockSize 50
struct Explosion
{
    int x;
    int y;
    int ticksLeft;
};
class Game
{
public:
    Game();
    void setBomberGuy(int block_x, int block_y, int blockSize = 50); //only game start
    void addEnemy(int block_x, int block_y, int type = 0,int blockSize = 50);           //only game start
    void addBomb(int x, int y);
    Bomb* getBomb(int number);
    size_t getBombsNumber();
    size_t getExplosionsNumber();
    Explosion* getExplosion(int number);
    int isWinner(); ///Wygrana 1, Nierozstrzygniete 0, Przegrana -1 //wywalanie potworów przy okazji
    void tick();
    void setBoomSound(Mix_Chunk *sound);
    Bomberguy* getBomberguy();
    int getEnemiesNumber();
    int getEnemyX(int number);
    int getEnemyY(int number);
    bool checkInBounds(int x, int y, int width, int height);
    static bool checkCoords(int x1, int y1, int x2, int y2);
    virtual ~Game();
protected:
private:

    Bomberguy bomberguy;
    Mix_Chunk *boomSound;
    std::vector<Bomb*>  bombs;
    std::vector<Enemy*> enemies;
    std::vector<Explosion*> explosions;
};

#endif // GAME_H
