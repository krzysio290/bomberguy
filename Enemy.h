#ifndef ENEMY_H
#define ENEMY_H
#include "Character.h"

class Enemy : public Character
{
    public:
        Enemy();
        virtual ~Enemy();
        void setX(int x);
        void setY(int y);
        int  getX();
        int  getY();
    protected:
    private:
};

#endif // ENEMY_H
