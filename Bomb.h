#ifndef BOMB_H
#define BOMB_H


class Bomb
{
public:
    Bomb();
    Bomb(int ticks);
    virtual ~Bomb();
    void arm();
    void disarm();
    bool isArmed();
    int getTimeLeft();
    void tick();
    void setX(int x);
    void setY(int y);
    int getX();
    int getY();
protected:
private:
    int timer;
    bool armed;
    int x;
    int y;
};

#endif // BOMB_H
