#include "Character.h"

Character::Character()
{
    block_x = 0;
    block_y = 0;
}

Character::~Character()
{

}

Character::Character(int horizontalBlock, int verticalBlock)
{
    block_x = horizontalBlock;
    block_y = verticalBlock;
}

int Character::getBlockX()
{
    return block_x;
}
int Character::getBlockY()
{
    return block_y;
}

void Character::move(Direction direction)
{
    switch(direction)
    {
    case UP:
        if(this->block_x%50 < 4)
        {
            this->block_x -= this->block_x%50;
        }
        else if(this->block_x%50 > 46)
        {
            this->block_x += 50-this->block_x%50;
        }
        this->block_y--;
#ifdef LOG
        std::cout << "Up key" << std::endl;
#endif //End LOG
        break;
    case DOWN:
        this->block_y++;
#ifdef LOG
        std::cout << "Down key" << std::endl;
#endif //End LOG
        break;
    case LEFT:
        this->block_x--;
#ifdef LOG
        std::cout << "Left key" << std::endl;
#endif //End LOG
        break;
    case RIGHT:
        this->block_x++;
#ifdef LOG
        std::cout << "Right key" << std::endl;
#endif //End LOG
        break;
    default:
        break;
    }
}

void Character::move(Direction direction, Map &map)
{
    switch(direction)
    {
    case UP:
        this->block_y--;
        this->block_y--;
#ifdef LOG
        std::cout << "Up key" << std::endl;
#endif //End LOG
        break;
    case DOWN:
        this->block_y++;
        this->block_y++;
#ifdef LOG
        std::cout << "Down key" << std::endl;
#endif //End LOG
        break;
    case LEFT:
        this->block_x--;
        this->block_x--;
#ifdef LOG
        std::cout << "Left key" << std::endl;
#endif //End LOG
        break;
    case RIGHT:
        this->block_x++;
        this->block_x++;
#ifdef LOG
        std::cout << "Right key" << std::endl;
#endif //End LOG
        break;
    default:
        break;
    }

}
