#ifndef MAP_H
#define MAP_H
#include <string>
#include <fstream>
#include <iostream>
#include <vector>

class Map
{
public:
    Map();
    void readMapFile(std::string filename);
    int getX();
    int getY();
    int getObjectXY(int x, int y);
    int getEnemiesNumber();
    int getEnemyX(int number);
    int getEnemyY(int number);
    virtual ~Map();
protected:
private:
    int objectsOnMap[50][50];
    int width;
    int height;
    struct EnemyPosition
    {
        int x;
        int y;
    };
    std::vector<EnemyPosition*> enemiesCoords;

};

#endif // MAP_H
