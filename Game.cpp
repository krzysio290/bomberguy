#include "Game.h"

Game::Game()
{
    boomSound = NULL;
}

Game::~Game()
{
    std::cout << "~Game()\n";
    for(size_t i = 0; i < enemies.size(); i++)
    {
        delete enemies[i];
    }
    for(size_t i = 0; i < bombs.size(); i++)
    {
        delete bombs[i];
    }
    for(size_t i = 0; i < explosions.size(); i++)
    {
        delete explosions[i];
    }
}
void Game::setBoomSound(Mix_Chunk *sound)
{
    boomSound = sound;
}

void Game::tick() ///ZROBIC CZYSZCZENIE JESLI TICKLEFT == 0
{
    ///WYKONAC RUCH WROGAMI
    for(unsigned int i=0; i<enemies.size(); i++)
    {
        if(rand()%2)
        {
            if(bomberguy.getBlockX()<enemies[i]->getX())
            {
                enemies[i]->setX(enemies[i]->getX()-1);
            }
            else
            {
                enemies[i]->setX(enemies[i]->getX()+1);
            }
        }
        else
        {
            if(bomberguy.getBlockY()<enemies[i]->getY())
            {
                enemies[i]->setY(enemies[i]->getY()-1);
            }
            else
            {
                enemies[i]->setY(enemies[i]->getY()+1);
            }
        }

    }
    ///Usuwanie zbednych zdarzen
    for(unsigned int i=0; i<explosions.size(); i++)
    {
        if(explosions[i]->ticksLeft==0)
        {
            //std::cout << "Usun szmate" << std::endl;
            delete explosions[i];
            explosions.erase(explosions.begin()+i);
        }
    }
    ///Usuniecie bomby
    for(unsigned int i=0; i<bombs.size(); i++)
    {
        if(bombs[i]->getTimeLeft()==0)
        {
            delete bombs[i];
            bombs.erase(bombs.begin()+i);
        }
    }
    ///Obsluga reszty
    for(unsigned int i=0; i<explosions.size(); i++)
    {
        if(explosions[i]->ticksLeft>0) explosions[i]->ticksLeft--;
    }
    for(unsigned int i=0; i<bombs.size(); i++)
    {
        if(bombs[i]->getTimeLeft() > 1)
        {
            bombs[i]->tick();
        }
        else
        {
            bombs[i]->tick();
            Explosion *expl = new Explosion;
            expl->x = bombs[i]->getX();
            expl->y = bombs[i]->getY();
            expl->ticksLeft = 50;
            explosions.push_back(expl);
            if(boomSound!=NULL) Mix_PlayChannel(1, boomSound, 0);
            ///DODAJ EKSPLOZJE - BOMBA SKONCZYLA ODLICZANIE
        }
    }
}
size_t Game::getExplosionsNumber()
{
    return explosions.size();
}

Explosion* Game::getExplosion(int number)
{
    return explosions[number];
}

void Game::addBomb(int x, int y)
{
    Bomb *bombka = new Bomb(200);
    bombka->setX(x);
    bombka->setY(y);
    bombka->arm();
    this->bombs.push_back(bombka);
}
Bomb* Game::getBomb(int number)
{
    return bombs[number];
}
size_t Game::getBombsNumber()
{
    return bombs.size();
}
void Game::setBomberGuy(int block_x, int block_y, int blockSize)
{
#ifdef LOG
    std::cout << "Added bomberguy" << std::endl;
#endif // LOG
    bomberguy.setX(block_x*blockSize);
    bomberguy.setY(block_y*blockSize);
}

Bomberguy* Game::getBomberguy()
{
    return &bomberguy;
}

int Game::getEnemiesNumber()
{
    return enemies.size();
}

int Game::getEnemyX(int number)
{
    return enemies[number]->getX();
}

int Game::getEnemyY(int number)
{
    return enemies[number]->getY();
}

void Game::addEnemy(int block_x, int block_y, int type, int blockSize)
{
    Enemy *opponent = new Enemy();
    opponent->setX(block_x*blockSize);
    opponent->setY(block_y*blockSize);
    this->enemies.push_back(opponent);
#ifdef LOG
    std::cout << "Added enemy" << std::endl;
#endif // LOG
}

bool Game::checkCoords(int x1, int y1, int x2, int y2)
{
    if((x1+50>=x2 && x1 <= x2) || (x2+50>=x1 && x2<=x1))
        if((y1+50>=y2 && y1<=y2) || (y2+50>=y1 && y2<=y1))
            return true; //nachodza na siebie
    return false;
}

bool Game::checkInBounds(int x, int y, int width, int height)
{
    if(width<100 || height<100)
    {
        width*=50;
        height*=50;
    }
    if(y >= 0 && y+50 <= height && x >= 0 && x+50 <= width) return true;
    return false;
}

int Game::isWinner() //wywalanie potworów
{
    for(unsigned int i = 0; i < enemies.size(); i++)
    {
        if(checkCoords(bomberguy.getBlockX(), bomberguy.getBlockY(), enemies[i]->getX(), enemies[i]->getY()))
        {
            return -1;
        }
    }
    for(unsigned int i = 0; i < explosions.size(); i++)
    {
        if(checkCoords(bomberguy.getBlockX(),bomberguy.getBlockY(), explosions[i]->x, explosions[i]->y))
        {
            return -1;
        }
        for(int j = enemies.size()-1; j >= 0 ; j--)
        {
            if( checkCoords(enemies[j]->getX(),enemies[j]->getY(), explosions[i]->x, explosions[i]->y) )
            {
                std::cout << "Enemy's down\n";
                delete enemies[j];
                enemies.erase(enemies.begin()+j);
            }
        }

        for(int explod_x = explosions[i]->x-100; explod_x < explosions[i]->x+100; explod_x+=50)
        {
            if(checkCoords(bomberguy.getBlockX(),bomberguy.getBlockY(), explod_x, explosions[i]->y))
            {
                return -1;
            }
            for(int j = enemies.size()-1; j >= 0 ; j--)
            {
                if( checkCoords(enemies[j]->getX(),enemies[j]->getY(), explod_x, explosions[i]->y) )
                {
                    std::cout << "Enemy's down\n";
                    delete enemies[j];
                    enemies.erase(enemies.begin()+j);
                }
            }
        }

        for(int explod_y = explosions[i]->y-100; explod_y < explosions[i]->y+100; explod_y+=50)
        {
            if(checkCoords(bomberguy.getBlockX(),bomberguy.getBlockY(), explosions[i]->x, explod_y))
            {
                return -1;
            }
            for(int j = enemies.size()-1; j >= 0 ; j--)
            {
                if( checkCoords(enemies[j]->getX(),enemies[j]->getY(), explosions[i]->x, explod_y) )
                {
                    std::cout << "Enemy's down\n";
                    delete enemies[j];
                    enemies.erase(enemies.begin()+j);
                }
            }
        }

    }
    for(unsigned int i = 0; i < enemies.size(); i++)
    {

    }
    if(enemies.size() == 0)
    {
        std::cout << "Wygral!" << std::endl;
        return 1;
    }
    return 0;
}
